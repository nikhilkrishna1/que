//
//  VerificationCodeViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 8/23/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class VerificationCodeViewController: UIViewController {
    
    


    @IBOutlet weak var verificationCodeInput: UITextField!
    
    let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
    
    var code = "invalid"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    // Mark: Actions
    
    //Get verification code:
    

    @IBAction func submitVerificationPressed(_ sender: Any) {
    
    
        if verificationCodeInput.text != nil {
            code = verificationCodeInput.text!
        }
        
        print("())()()*()()()()()()()!(@)(#)@!)(#)@!()#(@)!(#)!()!(#)!@()#(@!)(#)!@()9a", code)
        
//        code = "123756"
        
        if code.count == 6 {
            
            
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: code)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let _ = error {
                    // ...
                    print("ERROR SIGNING IN USER TO FIREBASE!")
                    return
                }
                // User is signed in
                // ...
                print("Signin Successful!")
            }
            
            print("%%%^^^^^^^^^^^^^^^^^^^^^^^^^^^^%%%%%")
            
        } else {
            
//            invalidCodeLabel.isHidden = false
            
            print("yuh oh")
            
            
            
            
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
//    func toMain() -> Void {
//        function body
//    }

}
