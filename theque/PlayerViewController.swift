//
//  PlayerViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 9/29/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
}

class TestViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

class MusicPlayer : UIViewController {
    
    var quePlayerView : PlayerViewControllerQue!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        var upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeResponse(gesture:)))
        upSwipe.direction = .up
        
        var downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeResponse(gesture:)))
        downSwipe.direction = .down
        
        self.view.addGestureRecognizer(downSwipe)
        self.view.addGestureRecognizer(upSwipe)
        
    }
    
    @objc func swipeResponse(gesture: UIGestureRecognizer) {
        
        if let swipeDirection = gesture as? UISwipeGestureRecognizer {
            
            switch swipeDirection.direction {
            
            case .down:
                print("Down swipe")
            case .up:
                print("WORK ON THIS NEXT")
                
                var quePlayerView = TestViewController()
                    //PlayerViewControllerQue()
                self.present(quePlayerView, animated: true)
            default:
                print("Somehow made it here, idk why default is nescessary and should learn that soon")
                
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
}

class PlayerViewControllerQue: UIViewController {
    
    
    var queData : [Que] = []
    
 
    
    //    @IBOutlet weak var playerImage: UIImageView!
    
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            print("did this")
            
            queData = getQue()
            
//            playerImage.image = queData[0].image
            
            //tableView.delegate = self
            //tableView.dataSource = self
            
            //Load QueCell xib
            //let nib = UINib(nibName: "QueCell", bundle: Bundle.main)
            //tableView.register(nib, forCellReuseIdentifier: "QueCell")
            
            //Load MusicPlayer xib
//            let headerNib = UINib.init(nibName: "MusicPlayer", bundle: Bundle.main)
            //tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "MusicPlayer")
            
            
            
            
            
            
//            tableView.isScrollEnabled = false

            
        }
    
    
    
    
    func getQue() -> [Que] {
        
        var arr : [Que] =  []
        
        print("made it here")
        
        let passImage : UIImage = #imageLiteral(resourceName: "150620_exphendrix")
        
        print("didn't make it here")
        
        for _ in 0...10 {
            print("Adding to array")
            arr.append(Que(image: passImage, title: "My Humps", artist: "Fergy"))
            
        }
        print(arr)
        return arr
    }
    

}

//extension PlayerViewControllerQue: UITableViewDataSource, UITableViewDelegate {
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return queData.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let queItem = queData[indexPath.row]
//
//
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "QueCell") as! QueCell
//
//
//
//
//        cell.songImage.image = queItem.image
//
//        cell.songTitle.text = queItem.title
//
//        cell.songArtist.text = queItem.artist
//
//        return cell
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MusicPlayer") as! MusicPlayer
//
//
//
//
//        headerView.songTitle.text = "hot dog"
//        headerView.songArtist.text = "hot dog"
//        headerView.songImage.image = #imageLiteral(resourceName: "150620_exphendrix")
//
//
//        return headerView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 500
//    }
//
//}


class QueCell: UITableViewCell {

    
    @IBOutlet var songImage: UIImageView!
    
    @IBOutlet var songTitle: UILabel!
    
    @IBOutlet var songArtist: UILabel!
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}

    
//class MusicPlayer : UIView {
//
//    var view : UITableViewHeaderFooterView!
//
//    @IBOutlet weak var songImage: UIImageView!
//
//    @IBOutlet weak var songTitle: UILabel!
//
//    @IBOutlet weak var songArtist: UILabel!
//
//    @IBOutlet weak var backwardButton: UIButton!
//
//    @IBOutlet weak var forwardButton: UIButton!
//
//    @IBOutlet weak var playButton: UIButton!
//
////    override init(frame: CGRect) {
////        super.init(frame: frame)
////        commonInit()
////    }
////
////    required init?(coder aDecoder: NSCoder) {
////        super.init(coder: aDecoder)
////        commonInit()
////    }
////
////    func commonInit() {
////        let bundle = Bundle.init(for: MusicPlayer.self)
////        if let viewsToAdd = bundle.loadNibNamed("MusicPlayer", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
////            addSubview(contentView)
////            contentView.frame = self.bounds
////            contentView.autoresizingMask = [.flexibleHeight,
////                                            .flexibleWidth]
////        }
////    }
//
//
//}

