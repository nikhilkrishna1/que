//
//  SpecificPublicQueTableViewCell.swift
//  theque
//
//  Created by Nikhil Krishna on 1/12/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit

class SpecificPublicQueTableViewCell: UITableViewCell {

    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumArt: UIImageView!
    @IBOutlet weak var numUpvotes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
