//
//  InitialViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 1/18/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class InitialViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener { auth, user in
          if let user = user {
            print(user.displayName)
            
            self.performSegue(withIdentifier: "LoggedIn", sender: nil)
            
          } else {
            // No user is signed in.
            print("User not signed in")
          }
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        
        
        self.performSegue(withIdentifier: "PresentLogin", sender: nil)
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Signup", bundle: nil)
        
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SignupViewController")
        
        
        self.performSegue(withIdentifier: "PresentSignup", sender: nil)
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
