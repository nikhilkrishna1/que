//
//  HomeCollectionCollectionViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 12/9/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

protocol CollectionCellController {
    
    static func registerCell(on collectionView: UICollectionView)
    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell
    func didSelectCell()
    
}


class HomeCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    

    fileprivate var cellControllers = [CollectionCellController]()
    
    //fileprivate var items = [HomeCellItems]
    
    fileprivate let cellControllerFactory = HomeCellFactory()
    
    fileprivate var items : [CellItems] = [
        CellItems.init(isSocial: false, isNav: true, isJoin: true, isCreate: false, isSong: false, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: true, isSong: false, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: true, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: false, isLiked: true, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: false, isLiked: false, isPlaylist: true)
    ]
    
    
    
    
    fileprivate let screenSize = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(40))

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //print("HomeCollectionViewController")
        
    
        self.collectionView.dataSource = self
        
        self.collectionView.delegate = self
        
        self.collectionView.isUserInteractionEnabled = true
        
        
        cellControllerFactory.registerCells(on: collectionView)
        
        
        cellControllers = cellControllerFactory.cellControllers(with: items)
        
        NotificationCenter.default.addObserver(self, selector: #selector(createQueSegue(_:)), name: Notification.Name("createQueButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(joinQueSegue(_:)), name: Notification.Name("joinQueButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(songPushToNav(_:)), name: Notification.Name("songButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(likedPushToNav(_:)), name: Notification.Name("likedButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playlistPushToNav(_:)), name: Notification.Name("playlistButtonTapped"), object: nil)
        
        
    }
    
    @objc func createQueSegue(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "MainToCreate", sender: nil)
        
    }
    
    @objc func joinQueSegue(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "MainToJoin", sender: nil)
    }
    
    @objc func songPushToNav(_ notification: Notification) {
        
        print("Does somethign to nav")
        //push to na
        
    }
    
    @objc func likedPushToNav(_ notification: Notification) {
        
        //push to na
        
    }
    
    @objc func playlistPushToNav(_ notification: Notification) {
        
        //push to na
        
    }


    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return cellControllers[indexPath.row].cellFromCollectionView(collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("In here")
        cellControllers[indexPath.row].didSelectCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return screenSize
    }


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

class HomeCellFactory {
    
    func registerCells(on collectionView: UICollectionView) {
        
        //register navigation and social feed cells
        NavigationButtonCellController.registerCell(on: collectionView)
    }
    
    func cellControllers(with items: [CellItems]) -> [CollectionCellController] {
        return items.map { item in
            
            if item.isNavigationButtonCellItem {
                return NavigationButtonCellController(item: item)
            } else {
                return NavigationButtonCellController(item: item)
            }
        }
        
    }
    
    
    
    
}


struct CellItems {
    var isSocialFeedItem: Bool
    
    var isNavigationButtonCellItem: Bool
    
    var isJoinQueItem : Bool
    
    var isCreateQueItem : Bool
    
    var isSongItem : Bool
    
    var isLikedItem : Bool
    
    var isPlaylistsItem : Bool
    
    init(isSocial : Bool, isNav : Bool, isJoin : Bool, isCreate : Bool, isSong : Bool, isLiked : Bool, isPlaylist : Bool) {
        self.isSocialFeedItem = isSocial
        self.isNavigationButtonCellItem = isNav
        self.isJoinQueItem = isJoin
        self.isCreateQueItem = isCreate
        self.isSongItem = isSong
        self.isLikedItem = isLiked
        self.isPlaylistsItem = isPlaylist
    }
    
}



//Top navigation controller for both registering xib to collection and modifying it for presentation
class NavigationButtonCellController : CollectionCellController {

    fileprivate let item : CellItems

    init(item: CellItems) {
        self.item = item
    }
    
    fileprivate static var cellIdentifier: String {
        return String(describing: type(of: MusicNavigationButtonView.self))
    }
    
    static func registerCell(on collectionView: UICollectionView) {

        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
        
        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
        
        
        
    }

    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
        
        //Do modification here
        
        cell.contentView.isUserInteractionEnabled = true
        
        cell.backgroundColor = UIColor.systemRed
        print(item)
        if item.isJoinQueItem {
            print("isJoinQueItem")
            
            
            cell.actionButton.setTitle("Join Que", for: .normal)
        } else if item.isCreateQueItem {
            cell.actionButton.setTitle("Create Que", for: .normal)
            
        } else if item.isSongItem {
            cell.actionButton.setTitle("Songs", for: .normal)
        } else if item.isLikedItem {
            cell.actionButton.setTitle("Liked", for: .normal)
        } else if item.isPlaylistsItem {
            cell.actionButton.setTitle("Playlists", for: .normal)
        } else {
            print("Shouldn't get a MusicNavigationButtonCell like this @ cellFromCollectionView")
        }
        
        return cell
        
    }

    func didSelectCell() {
        
        if item.isJoinQueItem {
            
            
            NotificationCenter.default
            .post(name:           NSNotification.Name("joinQueButtonTapped"),
             object: nil)
        } else if item.isCreateQueItem {
            
            NotificationCenter.default
            .post(name:           NSNotification.Name("createQueButtonTapped"),
             object: nil)
            
        } else if item.isSongItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("songButtonTapped"),
             object: nil)
        } else if item.isLikedItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("likedButtonTapped"),
             object: nil)
        } else if item.isPlaylistsItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("playlistButtonTapped"),
             object: nil)
        } else {
            print("Shouldn't get a Musa `icNavigationButtonCell like this")
        }
    }

}



class MusicNavigationButtonView : UICollectionViewCell {
   
    @IBOutlet weak var actionButton: UIButton!
    
    
}



//class JoinQueButtonCellController : CollectionCellController {
//
//    fileprivate let item : HomeCellItems
//
//    init(item: HomeCellItems) {
//        self.item = item
//    }
//
//    fileprivate static var cellIdentifier: String {
//        return String(describing: type(of: MusicNavigationButtonView.self))
//    }
//
//    static func registerCell(on collectionView: UICollectionView) {
//
//        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
//
//        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
//
//
//
//    }
//
//    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
//
//        //Do modification here
//
//        cell.backgroundColor = UIColor.systemRed
//
//
//
//        return cell
//
//    }
//
//    func didSelectCell() {
//        print("Selected NavigationButtonCell")
//    }
//
//}
//
//class CreateQueButtonCellController : CollectionCellController {
//
//    fileprivate let item : HomeCellItems
//
//    init(item: HomeCellItems) {
//        self.item = item
//    }
//
//    fileprivate static var cellIdentifier: String {
//        return String(describing: type(of: MusicNavigationButtonView.self))
//    }
//
//    static func registerCell(on collectionView: UICollectionView) {
//
//        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
//
//        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
//
//
//
//    }
//
//    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
//
//        //Do modification here
//
//        cell.backgroundColor = UIColor.systemRed
//
//
//
//        return cell
//
//    }
//
//    func didSelectCell() {
//        print("Selected NavigationButtonCell")
//    }
//
//}
//
