//
//  LoginViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 1/18/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    
    @IBOutlet weak var verificationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func verificationButtonTapped(_ sender: Any) {
    }
    
    @IBAction func loginFieldDidEdit(_ sender: Any) {
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class TextVerificationViewController : UIViewController {
    
    @IBOutlet weak var verifcationCodeField: UITextField!
    
    @IBOutlet weak var verifyButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    @IBAction func verificationCodeFieldDidEdit(_ sender: Any) {
    }
    
    @IBAction func verifyButtonTapped(_ sender: Any) {
    }
    
    
    
}
