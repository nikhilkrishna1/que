//
//  ExistingPublicQuesTableViewController.swift
//  theque
//
//  Copyright © 2019 Nikhil Krishna. All rights reserved.
//

import UIKit
import MapKit
import FirebaseFirestore
import CoreLocation

class ExistingPublicQueTableViewController: UITableViewController, CLLocationManagerDelegate, UISearchBarDelegate {

    

    @IBOutlet weak var radiusButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!

    var publicQues = [PublicQue]()
    var filteredQues = [PublicQue]()
    var db = Firestore.firestore()
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    var placemark: CLPlacemark!
    var regionLocation = BoundaryRegion(northWestCorner: CLLocationCoordinate2D(), southEastCorner: CLLocationCoordinate2D())
    var selectedPublicQue: PublicQue!
    var flag = true
    
    @objc func refresh(sender:AnyObject)
    {
        // Updating your data here...
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }


    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        radiusButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        self.tableView.reloadData()
        
        self.navigationItem.backBarButtonItem?.isEnabled = true
        
        loadPublicQues(aflag: self.flag)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.tableView.reloadData()
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        

        return self.publicQues.count

    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ExistingPublicQueTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExistingPublicQueTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ExistingPublicQueTableViewCell.")
        }
        
        
        let publicQue = publicQues[indexPath.row]
        cell.publicQueName.text = publicQue.name

            
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedPublicQue = publicQues[indexPath.row]
        self.performSegue(withIdentifier: "toSpecificPublicQue", sender: nil)

        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Optional("toSpecificPublicQue") {
            let specificPublicVC: SpecificPublicQueTableViewController = segue.destination as! SpecificPublicQueTableViewController
            specificPublicVC.publicQue = self.selectedPublicQue
            specificPublicVC.tableView.reloadData()
            
        }
    }
    

    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func loadPublicQues(aflag: Bool) {
        
        var neededPublicQues = [PublicQue]()
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if !(aflag) {
                    for document in querySnapshot!.documents {
                        
                        let publicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]])
                        
                        let location = CLLocationCoordinate2D(latitude: publicQue.latitude, longitude: publicQue.longitude)
                        
                        if (
                            location.latitude  >= self.regionLocation.northWestCorner.latitude &&
                            location.latitude  <= self.regionLocation.southEastCorner.latitude &&
            
                            location.longitude >= self.regionLocation.northWestCorner.longitude &&
                            location.longitude <= self.regionLocation.southEastCorner.longitude
                            ) {
                            neededPublicQues.append(publicQue)
                        }
                    }
                    self.publicQues = neededPublicQues
                    self.tableView.reloadData()
                } else {
                    self.citywide()
                    self.flag = false
                }
                
            }
        }
        
        self.tableView.reloadData()
        
    }

    
    @IBAction func searchRadius() {
        
        let alertController = UIAlertController(title: "Edit Search Radius", message: "Select a search radius to discover Public Ques nearby", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "City", style:
            UIAlertActionStyle.default) {
        UIAlertAction in
            
                self.citywide()
            
        })
        
        alertController.addAction(UIAlertAction(title: "Custom", style: UIAlertActionStyle.default) {
        UIAlertAction in
            
            self.customRadius()
            
        })
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)


        
    }
    
    // units are in feet
    func editRadius(radius: Int) {
        
        let db = Firestore.firestore()
        var neededPublicQues = [PublicQue]()
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let publicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]])
                    
    
                    let anotherLocation = CLLocation(latitude: publicQue.latitude, longitude: publicQue.longitude)
                    let currentLocation = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                    let distanceInMeters = currentLocation.distance(from: anotherLocation) // result is in meters
                
                    if (distanceInMeters * 3.28084) < Double(radius) {
                        neededPublicQues.append(publicQue)
                    }
                }
        
                self.publicQues = neededPublicQues
                self.tableView.reloadData()
            }
        }
    }
    
    func citywide() {
        let db = Firestore.firestore()
        self.publicQues = [PublicQue]()
        let currentCity = self.placemark.locality!
        let currentState = self.placemark.administrativeArea!
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let publicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]])
                    
                    if (document.get("city") as? String == currentCity
                        && document.get("state") as? String == currentState) {
                        self.publicQues.append(publicQue)
                        self.tableView.reloadData()
                    }
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func customRadius() {
        
        self.performSegue(withIdentifier: "toCustomRadius", sender: nil)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.userLocation.latitude = locValue.latitude
        self.userLocation.longitude = locValue.longitude
                
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: {
            
                placemarks, error -> Void in
                guard let placeMark = placemarks?.first else { return }
            
                self.placemark = placeMark
            
        })
        
    }
    
}

