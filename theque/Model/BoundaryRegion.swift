//
//  BoundaryRegion.swift
//  theque
//
//  Created by Nikhil Krishna on 1/8/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import MapKit

class BoundaryRegion {
    
    var northWestCorner: CLLocationCoordinate2D!
    var southEastCorner: CLLocationCoordinate2D!
    
    init(northWestCorner: CLLocationCoordinate2D, southEastCorner: CLLocationCoordinate2D) {
        
        self.northWestCorner = northWestCorner
        self.southEastCorner = southEastCorner
    }
    
    
    
}
