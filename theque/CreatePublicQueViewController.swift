//
//  CreatePublicQueViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/3/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import MapKit
import FirebaseFirestore
import FirebaseAuth
import Firebase

class CreatePublicQueViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {

    var db: Firestore!
    var locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    var userLocation = CLLocationCoordinate2D()
    var placemark: CLPlacemark!
    var isPublic = true
    
    @IBOutlet weak var publicQueName: UITextField!
    @IBOutlet weak var finishedButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var publicButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true

        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.publicQueName.delegate = self
        self.finishedButton.isEnabled = false
        self.finishedButton.alpha = 0.5
        
        self.navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        self.finishedButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)

    }
    
    
    @IBAction func initializePublic() {
          
        
        db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        
        db.collection("Public Ques").document(publicQueName.text!).setData([
            
            "name": publicQueName.text!,
            "latitude": self.userLocation.latitude,
            "longitude": self.userLocation.longitude,
            "city": self.placemark.locality!,
            "state": self.placemark.administrativeArea!,
            "songs": [self.song_to_dict(song:(Song(title: "test song title", artist: "test song artist")))],
            "members": [userID],
            "numUpvotes": [0],
            "likedSongUsersPublic": [[String:Bool]]()
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked")
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButton() {
        
        self.performSegue(withIdentifier: "backButtonInit", sender: nil)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.userLocation.latitude = locValue.latitude
        self.userLocation.longitude = locValue.longitude
                
        let userLocation = locations.last
        let viewRegion = MKCoordinateRegionMakeWithDistance((userLocation?.coordinate)!, 600, 600)
        
        self.mapView.setRegion(viewRegion, animated: true)
        
        self.annotation.coordinate = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        
        self.mapView.addAnnotation(annotation)
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: {
            
                placemarks, error -> Void in
                guard let placeMark = placemarks?.first else { return }
            
                self.placemark = placeMark
            
        })
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (self.publicQueName.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text.isEmpty {
            
            self.finishedButton.isEnabled = false
            self.finishedButton.alpha = 0.5
            
        } else {
            
            self.finishedButton.isEnabled = true
            self.finishedButton.alpha = 1.0
            
        }
        
        return true
    }
    
    //It's bullshit that I had to write this, Firebase does this for you?
//    func generateRandomStuff(length: Int) -> String {
//      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
//      return String((0..<length).map{ _ in letters.randomElement()! })
//    }

        
        
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func song_to_dict(song: Song) -> [String:Any] {
        
        let dict = ["title": song.title!, "artist": song.artist!] as [String : Any]
        return dict
        
    }
    
    @IBAction func toPrivateCreator() {

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PrivateQueCreator")
        self.show(vc, sender: self)
            
    }
    

}
