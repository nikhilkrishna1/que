//
//  SearchViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 11/30/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        
        
        let xib = UINib.init(nibName: "SearchResultsCell", bundle: nil)
        
        tableView.register(xib, forCellReuseIdentifier: "SearchResultsCell")
        
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //update results here
        
        //Need to implement iTunes to start on this
    }
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultsCell") as! SearchResultsCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //cell clicked, do something
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
}

class SearchResultsCell : UITableViewCell {
    
    @IBOutlet weak var resultsImage: UIImageView!
    
    @IBOutlet weak var resultsTitle: UILabel!
    
    @IBOutlet weak var resultsInfo: UILabel!
    
    
}
