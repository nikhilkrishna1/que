//
//  SpecificPublicQueTableViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/12/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class SpecificPublicQueTableViewController: UITableViewController {

    var publicQue = PublicQue()
    var songNames = [String]()
    var artistNames = [String]()
    var db = Firestore.firestore()
    var flag = false
    
    @objc func refresh(sender:AnyObject)
    {
        // Updating your data here...
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    @objc func addSongToPublic() {
        
        self.performSegue(withIdentifier: "toAddSongPublic", sender: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.title = publicQue.name
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.tableView.rowHeight = 75
        let userID = Auth.auth().currentUser!.uid


        // change connect bar button to add when connected to que
        if self.publicQue.members.contains(userID) {
            let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addSongToPublic))
            self.navigationItem.rightBarButtonItem = addBarButton
        }

        loadSongs()
    }




    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return songNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "SpecificPublicQueTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SpecificPublicQueTableViewCell  else {
            fatalError("The dequeued cell is not an instance of SpecificPublicQueTableViewCell.")
        }

        cell.songName.text = songNames[indexPath.row]
        cell.artistName.text = artistNames[indexPath.row]
        //CHANGE IMAGE LATER
        cell.albumArt.image = #imageLiteral(resourceName: "150620_exphendrix")
        cell.numUpvotes.text = String(self.publicQue.upvotes[indexPath.row])
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            
        let db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        
    
        let closeAction = UIContextualAction(style: .normal, title:  "Upvote", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
                       if let err = err {
                           print("Error getting documents: \(err)")
                       } else {
                           for document in querySnapshot!.documents {
                               if self.publicQue.name == document.get("name") as! String {
                                self.publicQue.upvotes[indexPath.row] += 1
                               }
                           }
                        db.collection("Public Ques").document(self.publicQue.name).updateData([
                            
                            "numUpvotes": self.publicQue.upvotes
                            
                        ]) {err in
                            if err != nil {
                                print("Somethin fucked up")
                            } else {
                                print("Database addition worked again")
                                self.tableView.reloadData()
                            }
                        }
                        
                    }
            }
            success(true)
        })
    
        closeAction.backgroundColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        return UISwipeActionsConfiguration(actions: [closeAction])
        
    }
        
        

    @IBAction func connectToSpecificPublicQue() {
        
        let userID = Auth.auth().currentUser!.uid
        let db = Firestore.firestore()
        
        
        db.collection("Public Users").document(userID).setData([
            
            "publicMember": true
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked")
            }
        }
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if self.publicQue.name == document.get("name") as! String {
                        print(userID)
                        self.publicQue.members.append(userID)
                    }
                }
                let alertController = UIAlertController(title: "Success.", message:
                    "Added to " + self.publicQue.name, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Done", style: .default))

                self.present(alertController, animated: true, completion: nil)
                let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addSongToPublic))
                self.navigationItem.rightBarButtonItem = addBarButton
                }
            
            }
        
        db.collection("Public Ques").document(self.publicQue.name).updateData([
            
            "members": FieldValue.arrayUnion([userID])
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked again")
            }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func loadSongs() {
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                
                    let newPublicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]])
                    
                    if newPublicQue.name == self.publicQue.name {
                        
                        self.songNames = self.getSongAttributes(publicQue: self.publicQue, attr: "title") as! [String]
                        self.artistNames = self.getSongAttributes(publicQue: self.publicQue, attr: "artist") as! [String]
                    }
                    
                    
                }
            }
            
            self.tableView.reloadData()
            
        }
        
        
        
    }
    
    func getSongAttributes(publicQue: PublicQue, attr: String) -> [String?] {

        var res = [String?]()

        if attr == "title" {
            for song in publicQue.songs {
                res.append(song["title"] as? String)
            }
        } else if attr == "artist" {
            for song in publicQue.songs {
                res.append(song["artist"] as? String)
            }
        }

        return res
        
    }
    
    
    
}
